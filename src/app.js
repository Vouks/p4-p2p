const IPFS = require('ipfs')
const Room = require('ipfs-pubsub-room')

const ipfs = new IPFS({
  repo: repo(),
  EXPERIMENTAL: {
    pubsub: true
  },
  config: {
    Addresses: {
      Swarm: [
        '/dns4/ws-star.discovery.libp2p.io/tcp/443/wss/p2p-websocket-star'
      ]
    }
  }
})
const room = Room(ipfs, 'ipfs-pubsub-demo')

ipfs.once('ready', () => ipfs.id((err, info) => {
  if (err) { throw err }
  document.getElementById("btn-chat").addEventListener("click", () => {broadcastMessage(info)})
  writeMessage(info.id+ ' entrou no chat')

  // Mostrando quando algum peer conecta ou desconecta

  room.on('peer joined', (peer) => writeMessage('peer ' + peer + ' joined'))
  room.on('peer left', (peer) => writeMessage('peer ' + peer + ' left'))

  // Enviando e recebendo mensagens

  room.on('peer joined', (peer) => room.sendTo(peer, 'Hello ' + peer + '!'))
  room.on('message', (message) => writeMessage(message.data.toString()))
}))



function repo () {
  return 'ipfs/pubsub-demo/' + Math.random()
}
// faz a mágica acontecer
function broadcastMessage(info) {
  room.broadcast(info.id + " diz: " + getMessageFromUser())
  cleanTextComponent()
}

// mostra a mensagem no chat
function writeMessage(message) {
  var chatComp = document.getElementById("chatId")
  var li = document.createElement("li")
  li.className = "left clearfix"
  li.textContent = message
  chatComp.appendChild(li)  
}

// Pegando a mensagem digitada pelo usuário
function getMessageFromUser() {
  var textComp = document.getElementById("texto")
  return textComp.value
}

// limpar a mensagem digitada pelo usuário
function cleanTextComponent() {
  document.getElementById("texto").value = ""
}
